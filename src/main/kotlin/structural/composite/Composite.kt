package structural.composite

import kotlin.random.Random

fun main() {
    val robots = MegaRobot(100)

    println("Baymax want to attack! Make figure!")
    var space = makeSpace()
    robots.makeFigure(Figure.X, space)
    printSpace(space)

    println("It's doesn't help. May be another figure?")
    space = makeSpace()
    robots.makeFigure(Figure.SLASH, space)
    printSpace(space)

    println("It's doesn't help too! Another figure!")
    space = makeSpace()
    robots.makeFigure(Figure.BACKSLASH, space)
    printSpace(space)

    println("Oh nooo, Baymax win!")
}

const val size = 10

typealias Space = Array<Array<Boolean>>

fun makeSpace() = Array(size) { Array(size) { false } }

fun printSpace(space: Space) {
    for (row in space) {
        for (cell in row) {
            print(if (cell) '*' else '-')
            print(' ')
        }
        println()
    }
}

enum class Figure { X, SLASH, BACKSLASH }

// -------- composite pattern itself:
interface Robot {
    fun makeFigure(figure: Figure, space: Space)
}

class NanoRobot : Robot {
    override fun makeFigure(figure: Figure, space: Space) {
        val pos = Random.nextInt(0, size)
        when (figure) {
            Figure.SLASH -> space[pos][pos] = true
            Figure.BACKSLASH -> space[pos][size - pos - 1] = true
            Figure.X -> if (Random.nextBoolean()) space[pos][pos] = true else space[pos][size - pos - 1] = true
        }
    }
}

class MicroRobot(count: Int) : Robot {
    private val nanoRobots = Array(count) { NanoRobot() }

    override fun makeFigure(figure: Figure, space: Space) {
        nanoRobots.forEach { it.makeFigure(figure, space) }
    }
}

class MegaRobot(count: Int) : Robot {
    private val microRobots = Array(count) { MicroRobot(count) }

    override fun makeFigure(figure: Figure, space: Space) {
        microRobots.forEach { it.makeFigure(figure, space) }
    }
}
