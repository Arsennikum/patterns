package structural.facade

import kotlin.random.Random

fun main() {
    HagridFacade().tameBuckbeak()
}

class HagridFacade : Human {
    fun tameBuckbeak() {
        println("Hagrid: Buckbeak good bird!!")
        Buckbeak.isFriendAfter(ActionForHippogriff.EYE_CONTACT, this)
        println("Hagrid: Yeah, Buckbeak proud bird!")
        while (!Buckbeak.isFriendAfter(ActionForHippogriff.BOWING, this)) {
            println("Hagrid: Buckbeak, that's all right, it's me")
        }
    }
}

interface Human

/**
 * гиппогриф Клювокрыл
 */
object Buckbeak {
    private val friends = mutableSetOf<Human>()
    private val contacters = mutableMapOf<Human, MutableSet<ActionForHippogriff>>()

    fun isFriend(human: Human) = friends.contains(human)

    fun isFriendAfter(action: ActionForHippogriff, human: Human): Boolean {
        if (isFriend(human)) return true
        return when (action) {
            ActionForHippogriff.EYE_CONTACT -> {
                contacters.getOrPut(human, ::mutableSetOf).add(action)
                false
            }
            ActionForHippogriff.BOWING -> if (contacters[human]?.contains(ActionForHippogriff.EYE_CONTACT) == true) {
                if (Random.nextBoolean() || Random.nextBoolean()) return false
                contacters.remove(human)
                friends += human
                true
            } else
                false
        }
    }
}

enum class ActionForHippogriff { EYE_CONTACT, BOWING }
