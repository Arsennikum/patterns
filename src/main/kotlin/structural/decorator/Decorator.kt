package structural.decorator

import kotlin.random.Random
import kotlin.random.nextInt

fun main() {
    println("Attack by new attacker!\n")
    var warrior: Attacker = SimpleAttacker("Warrior")
    warrior.attack()
    if (Random.nextBoolean()) {
        println("\nYou found artifact! What's it? Ruby? Trying to attack with it... \n")
        val boostedWarrior = RubyArtifactDecorator(warrior)
        boostedWarrior.attack()
        println("\nAnd now I have new attack: Ruby Attack!")
        boostedWarrior.rubyAttack()
        println("Ruby expired...")
    }
    warrior = if (Random.nextBoolean()) {
        DrStrangeAttacker(warrior)
    } else {
        GhostAttacker(warrior)
    }
    println("\nLast attack...")
    warrior.attack()
    println("\nGood game, ${warrior.name}")
}

interface Attacker {
    val name: String
    val power: Int
    fun attack()
}

class SimpleAttacker(override val name: String) : Attacker {
    override val power: Int
        get() = 10

    override fun attack() {
        println(">>$name attacked with power $power")
    }
}

/**
 * делегаты котлина позволяют не переопределять power и name поля
 */
class RubyArtifactDecorator(private val attacker: Attacker): Attacker by attacker {
    override fun attack() {
        println("I have double attack!")
        attacker.attack()
        attacker.attack()
    }

    fun rubyAttack() {
        println(">>Attack with ruby! Prepare...")
        Thread.sleep(100)
        println(">>Ruby attack with power ${power*3}")
    }
}
class GhostAttacker(private val attacker: Attacker) : Attacker by attacker {
    override val name: String
        get() =  "???"

    /**
     * Чтобы перезатёрлось name в методе, нужно переопределить и сам метод. Ведь это делегат
     */
    override fun attack() {
        println(">>$name attacked with $power")
    }
}

class DrStrangeAttacker(private val attacker: Attacker) : Attacker by attacker {
    override val power: Int
        get() = Random.nextInt(2..20)


    override fun attack() {
        println(">>$name attacked with power $power")
    }
}

