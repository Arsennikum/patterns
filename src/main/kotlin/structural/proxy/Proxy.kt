package structural.proxy

import kotlin.random.Random

fun main() {
    val string = HackedString("Hello World!")
    noddyFun(string)
}

fun noddyFun(string: CharSequence) {
    println("ow, I have a guest!\n")
    println(string)
}

class HackedString(private val string: String) : CharSequence by string {
    override fun toString(): String {
        if (Random.nextBoolean()) {
            return string
        }
        println("ha-ha, you're hacked!")
        return string.substring(0..string.length/3) + "... next part are encrypted! Pay to 777123 to get password"
    }
}
