package structural.flyweight

import creational.multiton.getSourceMultitonInstance
import kotlin.random.Random

fun main() {
    for (i in 0L..1000) {
        val source = if (Random.nextBoolean()) "google.com" else "ya.ru"
        println(Location(source, i, -i).getLocationInfo())
    }
}

class Location(
    sourceString: String,
    private val lat: Long,
    private val lon: Long
) {
    private val source = getSourceMultitonInstance(sourceString)

    fun getLocationInfo() = source.getLocationInfo(lat, lon)
}
