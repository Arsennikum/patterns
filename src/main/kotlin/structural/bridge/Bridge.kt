package structural.bridge

fun main() {
    val designerEffectively = DesignerDayReport(WorkingDay).calcEffectively() * 5 +
            DesignerDayReport(DayOff).calcEffectively() * 2
    println("week effectively of designer = $designerEffectively")

    val programmerEffectively = ProgrammerDayReport(WorkingDay).calcEffectively() * 5 +
            ProgrammerDayReport(DayOff).calcEffectively() * 2
    println("week effectively of programmer = $programmerEffectively")
}

sealed class DayReport(val dayType: DayType) {
    abstract fun calcEffectively(): Int
}

class DesignerDayReport(dayType: DayType): DayReport(dayType) {
    override fun calcEffectively() = dayType.calcWorkHours() + 5
}

class ProgrammerDayReport(dayType: DayType): DayReport(dayType) {
    override fun calcEffectively() = dayType.calcWorkHours()*2
}

sealed class DayType {
    abstract fun calcWorkHours(): Int
}

object WorkingDay : DayType() {
    override fun calcWorkHours() = 8
}

object DayOff: DayType() {
    override fun calcWorkHours() = 0
}
