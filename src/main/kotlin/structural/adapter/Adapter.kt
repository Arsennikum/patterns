package structural.adapter

fun main() {
    val waterCelsiusInfo: LiquidCelsiusInfo = WaterCelsiusInfo()
    println("${waterCelsiusInfo.boilingPoint()} boiling point")
    println("${waterCelsiusInfo.freezingPoint()} freezing point")

    println()
    println("Let's imagine, that's we have info only in fahrenheit:")
    val throughAdapter: LiquidCelsiusInfo = LiquidCelsiusInfoAdapter(WaterFahrenheitInfo())
    println("${throughAdapter.boilingPoint()} boiling point")
    println("${throughAdapter.freezingPoint()} freezing point")
}


class LiquidCelsiusInfoAdapter(private val liquidFahrenheitInfo: LiquidFahrenheitInfo) : LiquidCelsiusInfo {
    override fun boilingPoint() = fahrenheitToCelsius(liquidFahrenheitInfo.boilingPoint())

    override fun freezingPoint() = fahrenheitToCelsius(liquidFahrenheitInfo.freezingPoint())

    private fun fahrenheitToCelsius(fahrenheitTemp: FahrenheitTemp) =
        CelsiusTemp((fahrenheitTemp.temp - 32) * 5 / 9)
}

interface LiquidCelsiusInfo {
    fun boilingPoint(): CelsiusTemp
    fun freezingPoint(): CelsiusTemp
}

interface LiquidFahrenheitInfo {
    fun boilingPoint(): FahrenheitTemp
    fun freezingPoint(): FahrenheitTemp
}

class WaterCelsiusInfo : LiquidCelsiusInfo {
    override fun boilingPoint() = CelsiusTemp(100)
    override fun freezingPoint() = CelsiusTemp(0)
}

class WaterFahrenheitInfo : LiquidFahrenheitInfo {
    override fun boilingPoint() = FahrenheitTemp(212)
    override fun freezingPoint() = FahrenheitTemp(32)
}

data class CelsiusTemp(val temp: Int)
data class FahrenheitTemp(val temp: Int)
