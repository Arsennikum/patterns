package creational.factorymethod

import kotlin.random.Random

fun main() {
    println("Jumanji: Welcome to the Jungle")
    var jumanji: Jumanji = JumanjiOne()
    jumanji.playGame(Human.SPENCER)
    jumanji.playGame(Human.MARTHA)

    println()
    println("Jumanji: The Next Level")
    jumanji = JumanjiTwo()
    jumanji.playGame(Human.MARTHA)
    jumanji.playGame(Human.SPENCER)
}

enum class Human { SPENCER, MARTHA }

enum class HeroAvatar { BRAVESTONE, RUBY_ROUNDHOUSE }

/**
 * В первой части Джуманджи герои сами выбирали себе аватары,
 * а во второй они не успели выбрать - их закинуло случайным образом
 */
abstract class Jumanji {
    fun playGame(human: Human) {
        println("$human transforming...")
        val hero = createHero(human)
        println("Nigel: Hello in Jumanji, ${hero.name}")
    }

    abstract fun createHero(human: Human): HeroAvatar
}


class JumanjiOne : Jumanji() {
    override fun createHero(human: Human) = when (human) {
        Human.SPENCER -> HeroAvatar.BRAVESTONE
        Human.MARTHA -> HeroAvatar.RUBY_ROUNDHOUSE
    }
}

class JumanjiTwo : Jumanji() {
    override fun createHero(human: Human): HeroAvatar {
        val avatars = HeroAvatar.values()
        return avatars[Random.nextInt(0, avatars.size)]
    }
}
