package creational.multiton

import kotlin.random.Random

class SourceMultiton(
    private val connection: String
) {
    private val data: Array<String>

    init {
        println("connection initialization...")
        data = Array(1_000_000) { Random.nextInt().toString() }
    }

    fun getLocationInfo(lat: Long, lon: Long): String {
        println("trying to connect to $connection")
        return "point at $lat $lon"
    }
}

private val map: MutableMap<String, SourceMultiton> = HashMap()

fun getSourceMultitonInstance(connectionString: String) = map.computeIfAbsent(connectionString) {
    SourceMultiton(connectionString)
}
