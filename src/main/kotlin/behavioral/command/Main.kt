package behavioral.command

import kotlin.random.Random

fun main() {
    val reel = Reel(PriseCommand(), AaaautomobileCommand(), WordCommand(WordGenerator()))
    reel.executeSector(Random.nextInt(reel.wheelSize))
}

class Reel(private vararg val sectors: Command) {
    val wheelSize: Int
        get() = sectors.size

    fun executeSector(sectorIndex: Int) = sectors[sectorIndex].execute()
}

interface Command {
    fun execute()
}

class PriseCommand : Command {
    override fun execute() {
        println("Sector prise on reel!")
        println("${Random.nextInt()}$")
    }
}

class AaaautomobileCommand(private val yakub: String = "Yakub") : Command {
    override fun execute() {
        if (!yakub.startsWith("Yakub")) throw Exception("Only Yakubovich can aaa-aa-aautomobile this party")
        println("$yakub: Aaaaa-aaa-aaautomobile!")
    }

}

class WordCommand(private val wordGenerator: WordGenerator) : Command {
    override fun execute() {
        println("You must say word!")
        println(wordGenerator.generate())
    }
}

class WordGenerator {
    private val charPool: List<Char> = ('a'..'z') + ('A'..'Z')
    fun generate() = (1..5)
        .map { Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("")
}
