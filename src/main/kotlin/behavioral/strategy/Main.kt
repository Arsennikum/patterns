package behavioral.strategy

import kotlin.math.max
import kotlin.random.Random

fun main() {
    val firstPlayer = Player("firstPlayer", GunnerStrategy())
    val secondPlayer = Player("secondPlayer", SwordsmanStrategy())

    while (firstPlayer.healthy() && secondPlayer.healthy()) {
        firstPlayer.attack(secondPlayer)
        secondPlayer.attack(firstPlayer)

        firstPlayer.changeAttackStrategy(SwordsmanStrategy())
        firstPlayer.attack(secondPlayer)
        secondPlayer.attack(firstPlayer)

        firstPlayer.changeAttackStrategy(GunnerStrategy())
        firstPlayer.attack(secondPlayer)

        secondPlayer.changeAttackStrategy(GunnerStrategy())
        secondPlayer.attack(firstPlayer)
    }

    println(firstPlayer)
    println(secondPlayer)
    val winner = if (firstPlayer.health > secondPlayer.health) firstPlayer else secondPlayer
    print("Win: ${winner.name}")
}

class Player(
    val name: String,
    private var strategy: AttackStrategy
) {
    var health = 1000
        private set

    private val createTime = System.currentTimeMillis()

    private val power: Long
        get() = System.currentTimeMillis() - createTime

    fun changeAttackStrategy(strategy: AttackStrategy) {
        this.strategy = strategy
    }

    fun attack(enemy: Player) {
        println("$name: Prepare to attack...")
        strategy.attack(power, enemy)
    }

    fun damage(point: Float) {
        health = max(1000, (health * Random.nextDouble(1.0, 1.5)).toInt())
        health -= point.toInt()
    }

    fun healthy() = health > 0

    override fun toString() = "Player info: $name with health $health"
}

interface AttackStrategy {
    fun attack(power: Long, enemy: Player)
}

class GunnerStrategy : AttackStrategy {

    private fun calcAccuracy() = Random.nextFloat()

    override fun attack(power: Long, enemy: Player) {
        val accuracy = calcAccuracy()
        println("I will attack with $power power and $accuracy accuracy")
        enemy.damage(power * accuracy)
    }
}

class SwordsmanStrategy : AttackStrategy {

    private fun calcSwingOfSword() = Random.nextLong(700)

    override fun attack(power: Long, enemy: Player) {
        val swing = calcSwingOfSword()
        println("I will attack with $power power and $swing milliseconds of swing of the sword")
        Thread.sleep(swing)
        enemy.damage(power.toFloat())
    }
}
