package behavioral.iterator

import java.util.*

interface Reviewer {
    /**
     * шаблон накапливающий параметр. Не из GOF, но видел в TDD
     */
    fun review(collectingParameter: StringBuilder, reviewerIterator: ReviewerIterator)
}

interface ReviewerIterator {
    fun hasNext(): Boolean
    fun next(): Reviewer
}

class ReviewerBand {
    private val reviewerList = mutableListOf<Reviewer>()

    fun addReviewer(reviewer: Reviewer) = reviewerList.add(reviewer)

    fun iterator(index: Int = 0): ReviewerIterator = Iterator(index)

    private inner class Iterator(private var index: Int) : ReviewerIterator {
        init {
            if (index < 0 || index > reviewerList.size) throw IndexOutOfBoundsException("Index of behavioral.iterator: $index")
        }

        override fun hasNext() = index != reviewerList.size

        override fun next(): Reviewer {
            if (!hasNext()) throw NoSuchElementException()
            return reviewerList[index++]
        }

    }
}
