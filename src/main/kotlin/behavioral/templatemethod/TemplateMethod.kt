package behavioral.templatemethod


fun main() {
    println("Leo band on the road!")
    LeoBandTruck().rideOffRoad()
    println()
    println("Wolf band on the road!")
    WolfBandTruck().rideOffRoad()
}

abstract class Truck {
    /**
     * Шаблонный метод должен быть final. В котлине по умолчанию final
     */
    fun rideOffRoad() {
        prepare()
        putDriver()
        rideWithStyle()
    }

    protected abstract fun prepare()

    /**
     * методы могут иметь реализацию по умолчанию
     */
    protected open fun putDriver() {
        println("The driver got into the car")
    }

    protected abstract fun rideWithStyle()
}

class LeoBandTruck: Truck() {
    override fun prepare() {
        println("Draw car into Red")
    }

    override fun rideWithStyle() {
        println("Truck drive in realistic style with super speed")
    }
}

class WolfBandTruck: Truck() {
    override fun prepare() {
        println("Make cool wheel")
    }

    override fun rideWithStyle() {
        println("Truck drive with drift style")
    }
}
