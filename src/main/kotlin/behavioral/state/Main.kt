package behavioral.state

import kotlin.random.Random

fun main() {
    val truck = Truck(3)
    for (i in 0..100) {
        when(Random.nextInt(4)) {
            0 -> truck.drive()
            1 -> truck.putDriver()
            2 -> truck.loadCargo()
            3 -> truck.close()
        }
        if (truck.isWin()) {
            println("You're win!")
            return
        }
    }
}

class Truck(
    private val capacity: Int,
    private var state: TruckState = Init(capacity)
) {
    fun isWin() = state is Driving

    fun drive() {
        state = state.drive()
    }

    fun putDriver() {
        state = state.putDriver()
    }

    fun loadCargo() {
        state = state.loadCargo()
    }

    fun close() {
        state = state.close()
    }
}

interface TruckState {
    fun drive(): TruckState
    fun putDriver(): TruckState
    fun loadCargo(): TruckState
    fun close(): TruckState
}

class Init(private var capacity: Int) : TruckState {
    override fun drive(): TruckState {
        println("you can't drive without driver")
        return this
    }

    override fun putDriver(): TruckState {
        throw Exception("You put driver to empty truck. You need to pay money for him, while truck loading. You will go bankrupt")
    }

    override fun loadCargo(): TruckState {
        capacity -= 1
        if (capacity < 0) {
            throw Exception("Truck full. While you loading cargo truck have broken")
        }
        println("Cargo loaded")
        return this
    }

    override fun close(): TruckState {
        println("Ok, that's all. Closing")
        return Loaded()
    }
}

class Loaded : TruckState {
    override fun drive(): TruckState {
        throw Exception("You try to drive without driver? Truck crushed")
    }

    override fun putDriver(): TruckState {
        println("The driver got into the car")
        return LoadedWithDriver()
    }

    override fun loadCargo(): TruckState {
        println("you already closed the truck")
        return this
    }

    override fun close(): TruckState {
        println("you already closed the cargo")
        return this
    }
}

class LoadedWithDriver : TruckState {
    override fun drive(): TruckState {
        println("Yeah, you're driving")
        return Driving()
    }

    override fun putDriver(): TruckState {
        throw Exception("Are you want to drive truck with 2 driver for couple of hours? Then you will go bankrupt")
    }

    override fun loadCargo(): TruckState {
        println("You already loaded the cargo")
        return this
    }

    override fun close(): TruckState {
        println("You truck closed already")
        return this
    }
}

class Driving : TruckState {
    override fun drive(): TruckState {
        println("You already driving")
        return this
    }

    override fun putDriver(): TruckState {
        throw Exception("Driver try to catch truck, but unsuccessfully")
    }

    override fun loadCargo(): TruckState {
        throw Exception("Driver try to catch truck, but hit another truck")
    }

    override fun close(): TruckState {
        println("Truck driving and, hope, already closed")
        return this
    }
}
