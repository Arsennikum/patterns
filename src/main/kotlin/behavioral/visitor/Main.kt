package behavioral.visitor

fun main() {
    val visitors = arrayOf(
        Programmer(),
        Analytic(),
        Analytic(),
        Designer(),
        Designer(),
        Designer()
    )
    val visitables = arrayOf(AgileReport(), WaterfallReport())
    visitables.forEach {
        visitors.forEach(it::visit)
    }

    visitables.forEach(ReportVisitable::printTotalTime)
}
