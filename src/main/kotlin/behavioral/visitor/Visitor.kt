package behavioral.visitor

import kotlin.math.max

interface Visitor {
    fun visitAgile(visitable: ReportVisitable)
    fun visitWaterfall(visitable: ReportVisitable)
}

class Designer: Visitor {
    private val time = 5

    override fun visitAgile(visitable: ReportVisitable) {
        visitable.spendTime = max(visitable.spendTime, time)
    }

    override fun visitWaterfall(visitable: ReportVisitable) {
        visitable.spendTime += time
    }
}

class Programmer: Visitor {
    private val time = 10

    override fun visitAgile(visitable: ReportVisitable) {
        visitable.spendTime = max(visitable.spendTime, time)
    }

    override fun visitWaterfall(visitable: ReportVisitable) {
        visitable.spendTime += time
    }
}

class Analytic: Visitor {
    private val time = 15

    override fun visitAgile(visitable: ReportVisitable) {
        visitable.spendTime += time
    }

    override fun visitWaterfall(visitable: ReportVisitable) {
        visitable.spendTime += time
    }
}
