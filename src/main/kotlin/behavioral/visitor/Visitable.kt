package behavioral.visitor

sealed class ReportVisitable {
    var spendTime: Int = 0

    fun printTotalTime() {
        println("(${javaClass.simpleName}) time spend: $spendTime")
    }

    abstract fun visit(visitor: Visitor)
}

class AgileReport: ReportVisitable() {
    override fun visit(visitor: Visitor) = visitor.visitAgile(this)
}

class WaterfallReport: ReportVisitable() {
    override fun visit(visitor: Visitor) = visitor.visitWaterfall(this)
}
