package behavioral.chainofresponsibility

import behavioral.iterator.Reviewer
import behavioral.iterator.ReviewerBand
import behavioral.iterator.ReviewerIterator

/**
 * Здесь цепочка обязанностей применяется одновременно с итератором
 * В стандартных реализациях у цепочки есть метод setNext, но мне этот вариант больше нравится - меньше дублирования,
 * меньше вероятность ошибок с не установкой next
 */
fun main() {
    val reviewerIterator = ReviewerBand().apply {
        addReviewer(TeamLead())
        addReviewer(TechLead())
        addReviewer(ProjectManager())
        addReviewer(DevelopmentManager())
        addReviewer(Founder())
    }.iterator()
    reviewerIterator.nextReview(StringBuilder("Let's buy horizontal bar and swimming pool for Rocking Driving Development!\n"))
}

fun ReviewerIterator.nextReview(collectingParameter: StringBuilder) =
    if (hasNext()) next().review(collectingParameter, this)
    else {
        println()
        println("Review ready! Let's see it:")
        println(collectingParameter.toString())
    }


class TeamLead : Reviewer {
    override fun review(collectingParameter: StringBuilder, reviewerIterator: ReviewerIterator) {
        val name = this.javaClass.simpleName
        println("$name: This is wonderful")
        collectingParameter.append("$name: approved. ")
        reviewerIterator.nextReview(collectingParameter)
    }
}

class TechLead : Reviewer {
    override fun review(collectingParameter: StringBuilder, reviewerIterator: ReviewerIterator) {
        val name = this.javaClass.simpleName
        println("$name: This is great")
        collectingParameter.append("$name: approved. ")
        reviewerIterator.nextReview(collectingParameter)
    }
}

class ProjectManager : Reviewer {
    override fun review(collectingParameter: StringBuilder, reviewerIterator: ReviewerIterator) {
        val name = this.javaClass.simpleName
        println("$name: whaat? Let's say I didn't notice")
        collectingParameter.append("$name: approved. ")
        reviewerIterator.nextReview(collectingParameter)
    }
}

class DevelopmentManager : Reviewer {
    override fun review(collectingParameter: StringBuilder, reviewerIterator: ReviewerIterator) {
        val name = this.javaClass.simpleName
        println("$name: It's crazy, but ok")
        collectingParameter.append("$name: approved. ")
        reviewerIterator.nextReview(collectingParameter)
    }
}

class Founder : Reviewer {
    override fun review(collectingParameter: StringBuilder, reviewerIterator: ReviewerIterator) {
        val name = this.javaClass.simpleName
        println("$name: Ohh, why I must approve it?")
        collectingParameter.append("$name: approved. ")
        reviewerIterator.nextReview(collectingParameter)
    }
}
